import os
import subprocess
import time

detection_cmd = ["python", 
                 "classifier.py", 
                 "-f",
                 "trainingdata/training", 
                 "-vf", 
                 "trainingdata/validation",
                 "-s",
                 "vgg16.pt",
                 "-ep",
                 "10",
                 "-bs",
                 "64",
                 "-lr",
                 "0.005",
                 "--model_type",
                 "vgg16",
                 ]

subprocess.call(detection_cmd)

print("Training completed")
print("Now transfering files to S3")

if not os.path.isfile("training_losses.jpg"):
    print("something went wrong during the training loop")
    quit()

subprocess.call(["mv", "training_losses.jpg", "training_losses_vgg16.jpg"])
subprocess.call(["mv", "training_accuracy.jpg", "training_accuracy_vgg16.jpg"])
transfer_cmd1 = ["aws",
                 "s3",
                 "cp",
                 "vgg16.pt",
                 "s3://schoolbus-faces/"
                 ]
transfer_cmd2 = ["aws",
                 "s3",
                 "cp",
                 "training_losses_vgg16.jpg",
                 "s3://schoolbus-faces/"
                 ]
transfer_cmd3 = ["aws",
                 "s3",
                 "cp",
                 "training_accuracy_vgg16.jpg",
                 "s3://schoolbus-faces/"
                 ]
subprocess.call(transfer_cmd1)
subprocess.call(transfer_cmd2)
subprocess.call(transfer_cmd3)
print("Files transferred")

print("Now shutting down in 10 seconds")
time.sleep(10)

subprocess.call(["sudo", "poweroff"])