import tkinter as tk
from tkinter import messagebox


class Manager(object):
    """A manager object used for keeping track of events such as when
    a person has entered or exited the bus. The manager is also responsible
    for calling notification functions for certain events.

    Args:
        persons (list[KnownPerson]): Information on all the known persons
        unknown_threshold (float): The classification confidence threshold 
            below which the person is registered as 'Unknown'
        messaging (bool): True if messaging should be enabled. If False 
            notifications to the bus manager and the parents are
            disabled
        require_confirmation (bool): If True sending a notification to the
            parents the message should first go through the manager for 
            approval.

    Attributes:
        known (list[KnownPerson]): Information on all the known persons
        seen_entering (dict): A dictionary storing the seen count for each
            known person entering
        seen_exiting (dict): A dictionary storing the seen count for each
            known person exiting
        stranger_entered (int): A counter keeping track of the amount of 
            unidentified persons that entered
        stranger_exited (int): A counter keeping track of the amount of 
            unidentified persons that exited
        unknown_threshold (float): The classification confidence threshold 
            below which the person is registered as 'Unknown'
        messaging (bool): True if messaging should be enabled. If False 
            notifications to the bus manager and the parents are
            disabled
        require_confirmation (bool): If True sending a notification to the
            parents the message should first go through the manager for 
            approval.
        arrived (bool): True if the final destination of the bus has been reached
            and it it time to do a headcount.

    """

    def __init__(self, 
                 persons, 
                 unknown_threshold=0.5,
                 messaging=True,
                 require_confirmation=False):
        self.known = persons
        self.seen_entering = {person: 0 for person in persons}
        self.seen_exiting = {person: 0 for person in persons}
        self.strangers_entered = 0
        self.strangers_exited = 0
        self.unknown_threshold = unknown_threshold
        self.messaging_enabled = messaging
        self.require_confirmation = require_confirmation
        self.arrived = False


    def add_enter_observation(self, tracker):
        """Update the manager with a person that has been seen entering

        Args:
            tracker (PersonTracker): The tracker that has enough detections to be
                counted as an observation.
        """
        cid, conf = tracker.predict()
        if conf < self.unknown_threshold or cid == 0:
            self.strangers_entered += 1
            general_notification("An unrecognized person has entered the bus.")
            if self.messaging_enabled:
                notify_manager("An unidentified person has entered the bus.")

        else:
            self.seen_entering[self.known[cid]] += 1
            general_notification(f"{str(self.known[cid])} has entered the bus.")

            
    def add_exit_observation(self, tracker):
        """Update the manager with a person that has been seen exiting

        Args:
            tracker (PersonTracker): The tracker that has enough detections to be
                counted as an observation.
        """
        cid, conf = tracker.predict()
        if conf < self.unknown_threshold or cid == 0:
            self.strangers_exited += 1
            general_notification("An unrecognized person has left the bus.")
        else:
            self.seen_exiting[self.known[cid]] += 1
            general_notification(f"{str(self.known[cid])} has left the bus.")



    def final_stop(self):
        """Do a final headcount of all the people that entered and exited to see if anyone was
        missed. Also determine which known person might still be on the bus and which person 
        known person might not have shown up on the bus.
        If enabled send the appropreate messages to the bus manager and/or parents
        """
        if not self.messaging_enabled:
            return
        # If a person has not been seen either when entering or exiting, he is counted as a no-show
        seen = [p for p, count in self.seen_entering.items() if count > 0]
        seen += [p for p, count in self.seen_exiting.items() if count > 0]
        no_shows = [p for p in self.known if p not in seen]
        del no_shows[0] # Remove the unknown person


        if no_shows:
            m_msg = ["The following persons were not detected on the bus ride:"]
            for ns in no_shows:
                m_msg.append(str(ns))
                p_msg = ("This is an automated message from our schoolbus absence detection system. "
                         "We regret to inform you that your child {} did not appear on the "
                         "schoolbus today.").format(str(ns))
                notify_parents(p_msg, self.messaging_enabled, self.require_confirmation)
            notify_manager("\n".join(m_msg), self.messaging_enabled)

        entered = [p for p, count in self.seen_entering.items() if count > 0]
        exited = [p for p, count in self.seen_exiting.items() if count > 0]

        left = [p for p in entered if p not in exited]

        if left:
            msg = ["The following people may still be left in the bus"]
            for le in left:
                msg.append(str(le))
            notify_manager("\n".join(msg), self.messaging_enabled)

        enter_count = sum(self.seen_entering.values()) + self.strangers_entered
        exit_count = sum(self.seen_exiting.values()) + self.strangers_exited

        general_notification(f"The headcount is: {enter_count} people entered and {exit_count} people exited.")



#########################################################################################################
# The behavior of these notification functions can be extended as needed (with email, sms handling etc) #
#########################################################################################################

def general_notification(msg):
    """A general message to show in the final app or just as output in the console.

    Args:
        msg (str): Message to display
    """
    print(msg)


def notify_manager(msg, enabled=True):
    """A message that should be sent to the bus manager only.

    Args:
        msg (str): Message to display
        enabled (bool): If False the manager notification will be suppressed
    """
    if enabled:
        print()
        print("--- MANAGER NOTIFICATION ---")
        print(msg)
        print("----------------------------")
        print()
        


def ask_manager(msg):
    """Before sending a notification to the parents the message should 
    first go through the manager to approve.

    Args:
        msg (str): The message to be approved.

    Returns:
        bool: True if the message is approved, False otherwise
    """

 
    mmsg = "Approve the following message and send to the parents?\n\n"

    tk.Tk().withdraw()
    answer = messagebox.askyesno("Manager confirmation", mmsg+msg)
    return answer
   


def notify_parents(msg, enabled=True, confirmation=False):
    """A message that should be sent to the parents of a person.

    Args:
        msg (str): Message to display
        enabled (bool): If False the parent notification will be suppressed
        confirmation (bool): If True sending a notification to the
            parents the message should first go through the manager for 
            approval.
    """
    if enabled:
        askmsg = "Send the following message to the parents? \n\n" + msg
        proceed = True
        if confirmation:
            proceed = ask_manager(askmsg)
        if proceed:
            send_parents(msg)


def send_parents(msg):
    """Dummy function to be implemented in combination with a final app"""
    print()
    print("--- PARENT MESSAGE ---")
    print(msg)
    print("----------------------")
    print()



def send_email(msg, email):
    """Dummy function to be implemented in combination with a final app"""

    pass

def send_sms(msg, telephone):
    """Dummy function to be implemented in combination with a final app"""

    pass
