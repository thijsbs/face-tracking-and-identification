import subprocess
import tkinter as tk
from tkinter import * 
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename, askdirectory




class CustomWidget(tk.Frame):
    def __init__(self, parent, label, default=""):
        tk.Frame.__init__(self, parent)

        self.label = tk.Label(self, text=label, anchor="w")
        self.entry = tk.Entry(self)
        self.entry.insert(0, default)

        self.label.pack(side="left", fill="x")
        self.entry.pack(side="right", fill="x", padx=4)

    def get(self):
        return self.entry.get()

    def __str__(self):
        return self.entry.get()


class CustomDirectory(tk.Frame):
    def __init__(self, parent, label, default=""):
        tk.Frame.__init__(self, parent)

        def pick_file():
            self.entry.delete(0,tk.END)

            self.entry.insert(0, askdirectory())

        self.label = tk.Label(self, text=label, anchor="w")
        self.entry = tk.Entry(self)
        self.button = Button(self, text="Pick", command=pick_file)
        self.entry.insert(0, default)

        self.label.grid(row=0, column=0)
        self.entry.grid(row=0, column=1)
        self.button.grid(row=0, column=2)

    def get(self):
        return self.entry.get()

    def __str__(self):
        return self.entry.get()


class CustomFile(tk.Frame):
    def __init__(self, parent, label, default=""):
        tk.Frame.__init__(self, parent)

        def pick_file():
            self.entry.delete(0,tk.END)
            self.entry.insert(0, askopenfilename())

        self.label = tk.Label(self, text=label, anchor="w")
        self.entry = tk.Entry(self)
        self.button = Button(self, text="Pick", command=pick_file)
        self.entry.insert(0, default)

        self.label.grid(row=0, column=0)
        self.entry.grid(row=0, column=1)
        self.button.grid(row=0, column=2)

    def get(self):
        return self.entry.get()

    def __str__(self):
        return self.entry.get()


class CustomCheckbox(tk.Frame):
    def __init__(self, parent, label, default=False):
        tk.Frame.__init__(self, parent)

        self.val = IntVar()
        self.val.set(default)
        self.cb = tk.Checkbutton(self, text=label, variable=self.val)

        self.cb.pack(side="left", fill="x")


    def get(self):
        return self.val.get()

    def __str__(self):
        return str(self.val.get())



root = tk.Tk()
# root.bind("<Button>", button_click_exit_mainloop)
root.geometry('600x600')

opt = {
       # "test_device": CustomWidget(root, "Device type", default="cuda:0"),
       "classifier_path": CustomFile(root, "Classifier file", default="classifier_weights.pt"),
       "enter_camera": CustomFile(root, "Enter camera stream", default="None"),
       "exit_camera": CustomFile(root, "Exit camera stream", default="None"),
       "csv": CustomFile(root, "Student CSV file", default="student_data.csv"),
       "box_similarity_threshold": CustomWidget(root, "Box similarity threshold", default="50"),
       "output_dir": CustomDirectory(root, "Output frames directory", default="out_frames/"),
       "faces_dir": CustomDirectory(root, "Output faces directory", default="extracted_faces/"),
       "face_detection_threshold": CustomWidget(root, "Face detection threshold", default="0.7"),
       "classification_threshold": CustomWidget(root, "Classification threshold", default="0.5"),
       "frame_continuity_threshold": CustomWidget(root, "Frame continuity threshold", default="50"),
       "face_output_border": CustomWidget(root, "Additional border around face", default="0.1"),
       "no_classification": CustomCheckbox(root, "Disable classification"),
       "save_frames": CustomCheckbox(root, "Save output frames"),
       "save_faces": CustomCheckbox(root, "Save detected faces as images"),
       "no_show": CustomCheckbox(root, "Hide video feed"),
       "fps": CustomCheckbox(root, "Print the fps"),
       "sequential_streams": CustomCheckbox(root, "Load the exit camera after the enter camera stopped"),
       "require_message_confirmation": CustomCheckbox(root, "Require confirmation before sending messages"),
       "messaging": CustomCheckbox(root, "Enable messaging demo (command line output)"),
            }


# set defaults


gpu = IntVar()
gpu.set(True)
c3 = tk.Checkbutton(root, text="Run on GPU", variable=gpu)

i=0
# opt["test_device"].grid(row = i, column = 0, pady = 2); i+=1
opt["enter_camera"].grid(row = i, column = 0, pady = 2); i+=1
opt["exit_camera"].grid(row = i, column = 0, pady = 2); i+=1
opt["classifier_path"].grid(row = i, column = 0, pady = 2); i+=1
opt["csv"].grid(row = i, column = 0, pady = 2); i+=1
opt["box_similarity_threshold"].grid(row = i, column = 0, pady = 2); i+=1
opt["output_dir"].grid(row = i, column = 0, pady = 2); i+=1
opt["faces_dir"].grid(row = i, column = 0, pady = 2); i+=1
opt["face_detection_threshold"].grid(row = i, column = 0, pady = 2); i+=1
opt["classification_threshold"].grid(row = i, column = 0, pady = 2); i+=1
opt["frame_continuity_threshold"].grid(row = i, column = 0, pady = 2); i+=1
opt["face_output_border"].grid(row = i, column = 0, pady = 2); i+=1
c3.grid(row = i, column = 0, pady = 2); i+=1
opt["no_show"].grid(row = i, column = 0, pady = 2); i+=1
opt["no_classification"].grid(row = i, column = 0, pady = 2); i+=1
opt["save_frames"].grid(row = i, column = 0, pady = 2); i+=1
opt["save_faces"].grid(row = i, column = 0, pady = 2); i+=1
opt["fps"].grid(row = i, column = 0, pady = 2); i+=1
opt["sequential_streams"].grid(row = i, column = 0, pady = 2); i+=1
opt["messaging"].grid(row = i, column = 0, pady = 2); i+=1
opt["require_message_confirmation"].grid(row = i, column = 0, pady = 2); i+=1



def launch():
    cmd = ["python", "main.py"]
    if not gpu.get():
        cmd.append("--test_device")
        cmd.append("cpu")
    for flag, val in opt.items():
        if isinstance(val, CustomCheckbox):
            if val.get():
                cmd.append(f"--{flag}")
        else:
            cmd.append(f"--{flag}")
            cmd.append(val.get())
    print("The following command will be run:")
    print()
    print(repr(" ".join(cmd)))
    print()
    subprocess.Popen(cmd)


b1 = Button(root, text="Run", command=launch)
b2 = Button(root, text="Cancel", command=root.destroy)
b1.grid(row = i, column = 0, pady = 2) 
b2.grid(row = i, column = 1, pady = 2) 


# w = OptionMenu(master, variable, "one", "two", "three")




# for i in range(z_size):
#     w = tk.Scale(frame, from_=-1, to=1, 
#                       resolution=0.1, 
#                       length=180,
#                       orient=tk.HORIZONTAL, 
#                       showvalue=0,
#                       command=lambda x, idx=i: update(idx, x))
#     w.set(float(z[0][i]))
#     w.pack()

# canvas.create_window(150, 150, anchor='center', window=frame)
# canvas.update_idletasks()
# canvas.configure(scrollregion=(0, -8*z_size, 0, 12*z_size), 
#                  yscrollcommand=scroll_y.set) 
# canvas.pack(fill='both', expand=True, side="left")
# scroll_y.pack(fill='y', side='right')

root.mainloop()