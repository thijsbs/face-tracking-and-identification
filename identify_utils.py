import os

import argparse
from shutil import copyfile
from tkinter import * 
from tkinter.ttk import *

import person
  
parser = argparse.ArgumentParser(
    description='You can use this GUI to help group unknown face identifications to generate the training set.')

parser.add_argument("-f", "--folder", required=True, type=str,
                    help="The directory where the raw face tracking output is stored")
parser.add_argument("-o", "--output", required=True, type=str,
                    help="The output directory where the images are added to the training dataset.")
parser.add_argument("-c", "--csv", default="student_data.csv", type=str,
                    help="The path to the student information CSV file")

def start_ui(base, dropdown_names, target_folder):
    # creating main tkinter window/toplevel 

    def close_window(): 
        master.destroy()

    dropdowns = {}

    master = Tk() 
    folders = next(os.walk(base))[1]
    for i, fol in enumerate(folders):
        # this wil create a label widget 
        l = Label(master, text = fol) 
          
        # grid method to arrange labels in respective 
        # rows and columns as specified 
        l.grid(row = i, column = 0, sticky = W, pady = 2) 
        dropdown = StringVar(master)
        dropdown.set(dropdown_names[0])
        # entry widgets, used to take entry from user 
        d = OptionMenu(master, dropdown, *dropdown_names)
        d.grid(row = i, column = 1, pady = 2) 
        dropdowns[fol] = dropdown
        
    apply_cmd = lambda d=dropdowns, s=base, t=target_folder, c=close_window: copy_files(d, s, t, c)
    b1 = Button(master, text="Apply", command=apply_cmd)
    b2 = Button(master, text="Cancel", command=close_window)
    b1.grid(row = len(folders), column = 0, pady = 2) 
    b2.grid(row = len(folders), column = 1, pady = 2) 

      
    # infinite loop which can be terminated by keyboard 
    # or mouse interrupt 
    mainloop() 

def copy_files(drpds, source, target, close_cmd):

    for person, itm in drpds.items():
        files = next(os.walk(os.path.join(source, person)))[2]
        name = itm.get()
        print(name)
        if name == "Ignore":
            continue
        target_dir = os.path.join(target, name)
        if not os.path.isdir(target_dir):
            os.mkdir(target_dir)
        idx = len(next(os.walk(target_dir))[2])
        for file in files:
            fp_s = os.path.join(source, person, file)
            fp_t = os.path.join(target_dir, f"{idx}.jpg")
            print(f"Copying  from: {fp_s}\n"
                  f"           To: {fp_t}\n")
            copyfile(fp_s, fp_t)
            idx += 1
    close_cmd()


if __name__ == "__main__":

    args = parser.parse_args()
    available_persons = person.known_person_list(args.csv)
    dropdown_names = ["Ignore"]*2
    dropdown_names += [str(p) for p in available_persons]
    start_ui(args.folder, dropdown_names, args.output)

