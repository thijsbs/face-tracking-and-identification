import os
import random
import numpy as np
from math import sqrt, inf
from datetime import datetime
from scipy.spatial.distance import cosine


def known_person_list(csv):
    """Load the csv file with student information

    Args:
        csv (str): The file path to the csv file

    Returns:
        list[KnownPerson]
    """

    if csv[-3:] != "csv":
        raise IOError("The provided spreadsheet is not a .csv file.")

    with open(csv, "r") as f:
        items = [row.strip("\n").split(",") for row in f.readlines()]
    keys = items[0]
    required = [tuple(itm[:2] + [i+1]) for i, itm in enumerate(items[1:])]
    optional = [dict(zip(keys[2:], itm[2:])) for itm in items[1:]]
    # Add a person stranger
    required = [("Unknown", "", 0)] + required
    optional = [{}] + optional
    persons = [KnownPerson(*required[i], **optional[i]) for i in range(len(required))]
    return persons


class KnownPerson():
    """A class containing information about each known person

    Args:
        first_name (str): The first name of the person
        last_name (str): The last name of the person
        clf_id (int): The class ID of the person
        **kwargs: Other information about the person to store

    Attributes:
        first_name (str): The first name of the person
        last_name (str): The last name of the person
        clf_id (int): The class ID of the person
        info (dict): Other stored information about the person 
        """

    def __init__(self, first_name, last_name, clf_id, **kwargs):
        self.first_name = first_name
        self.last_name = last_name
        self.clf_id = int(clf_id)
        self.info = kwargs

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return str(self) == str(other)
        return False

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(str(self))


class PersonTracker():
    """A class that governs a single tracked person. A tracked person
    is a collection of detections that likely belong to the same person

    Args:
        bbox (tensor):Two coordinates defining the position and size of 
            the current face bounding box
        prediction (numpy array): The face prediction tensor of the 
            current prediction
        face (PIL.Image): The face image of the currently detected face
        frame_nr (int): The current frame number

    Attributes:
        bbox (tensor):Two coordinates defining the position and size of 
            the last face bounding box
        history (list[tensor]): All the previous face bounding boxes that
            have been related to this tracker
        faces (list[PIL.Image]): A list of all the extracted face images
            that have been related to this tracker
        predictions (list[numpy array]): A list of face prediction tensors
            that have been related to this tracker
        detection_count (int): The amount of detections that have been 
            related to this tracker
        tracker_id (str): The unique ID of the tracker
        last_seen (int): The frame number at which the tracker was last
            seen
        taken (bool): True if the tracker has already been related to one
            of the current detections. False otherwise
        cold (bool): True if the tracker has been lost more than the allowed
            time. No new detections will be related to this tracker if True
    """
    def __init__(self, bbox, prediction=None, face=None, frame_nr=0):
        self.bbox = bbox
        self.history = []
        self.faces = [] if face is None else [face]
        self.predictions = [prediction] if prediction is not None else []
        self.detection_count = 0
        self.tracker_id = "{:06}".format(random.randint(0, 999999))
        self.last_seen = frame_nr
        self.taken = False
        self.cold = False


    @property
    def avg_prediction(self):
        """The average of all the stored prediction tensors"""
        if not self.predictions:
            return None
        return sum(self.predictions) / len(self.predictions)


    def predict(self):
        """Take the average of the classifications.

        Returns:
            int: The class ID of the average prediction
            float: The confidence of the average prediction
        """
        if not self.predictions:
            return "Unknown", 0.
        class_id = np.argmax(self.avg_prediction)
        conf = np.max(self.avg_prediction)
        return class_id, conf


    def update(self, bbox, face_image=None, prediction=None, frame_nr=0):
        """Update the tracker with a new detection

        Args:
            bbox (tensor):Two coordinates defining the position and size of 
                the current face bounding box
            prediction (numpy array): The face prediction tensor of the 
                current prediction
            face_image (PIL.Image): The face image of the currently detected face
            frame_nr (int): The current frame number
        """
        self.history.append(self.bbox)
        self.bbox = bbox
        self.detection_count += 1
        self.last_seen = frame_nr
        self.taken = True
        if face_image is not None:
            self.faces.append(face_image)
        if prediction is not None:
            self.predictions.append(prediction)


    def save_faces(self, output_dir):
        """Save all the stored faces belonging to this tracker as image
        files.

        Args:
            output_dir (str): The directory where the faces should be saved
        """

        if not self.faces:
            return
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        person_dir = os.path.join(output_dir, f"person_{self.tracker_id}")
        os.mkdir(person_dir)

        for i, face in enumerate(self.faces):
            face.save(os.path.join(person_dir, f"{i}.jpg"))



class PersonTrackerCollection(list):
    """A class defining a collection of trackers in the current session

    Args:
        manager_callback(function): The function that should be called if a
            tracker is lost
        consecutive_frame_threshold (int): The minimum amount of detections
            required to store a lost tracker as an event.
        lost_threshold (int): The maximum amount of consequtive frames without
            an update to the tracker before the tracker is given up for lost.

    Attributes:
        manager_callback(function): The function that should be called if a
            tracker is lost
        consecutive_frame_threshold (int): The minimum amount of detections
            required to store a lost tracker as an event.
        lost_threshold (int): The maximum amount of consequtive frames without
            an update to the tracker before the tracker is given up for lost.
             
    """

    def __init__(self, manager_callback, consecutive_frame_threshold=10, lost_threshold=50):
        super(PersonTrackerCollection, self).__init__()
        self.manager_callback = manager_callback
        self.lost_threshold=lost_threshold
        self.consecutive_frame_threshold = consecutive_frame_threshold


    def get_tracker(self, tracker_id):
        """Get a tracker from the collection from its ID

        Args:
            tracker_id (str): The tracker ID of the requeste tracker

        Rturns:
            PersonTracker
        """
        for tr in self:
            if tr.tracker_id == tracker_id:
                return tr
        return None


    def query(self, bbox, pred=None, thresh=20.):
        """Find a matching tracker based on a bounding box

        Args:
            bbox (tensor):Two coordinates defining the position and size of 
                the face bounding box to match
            pred (numpy array): The prediction tensor of the face to match.
                This input is currently not being used to match the 
                bounding box
            thresh: (float): The maximum dissimilarity metric above which 
                a bounding box is not considered for a match.

        Returns:
            PersonTracker

        """
        if not self:
            return None

        def avdist(bbox1, bbox2):
            """A dissimilarity heuristic based on position and size""" 
            dist1 = sqrt((bbox1[0]-bbox2[0])**2 + (bbox1[1]-bbox2[1])**2)
            dist2 = sqrt((bbox1[2]-bbox2[2])**2 + (bbox1[3]-bbox2[3])**2)
            return (dist1 + dist2) / 2

        # Tracking continuity heuristic:
        distances = [inf if p.cold or p.taken else avdist(bbox, p.bbox) for p in self]

        # My idea is to take into account the latest prediction vector and compare it to the tracked persons
        # with cosine similarity. It wasn't giving me results I wanted right away so I decided to leave
        # it. It might still be a good idea to pick this up again
        # cosine_similarity = [0. if not p.predictions else 1-cosine(pred, p.avg_prediction) for p in self]
        # heuristic = [distances[i]+20*cosine_similarity[i] for i in range(len(self))]
        heuristic = distances
        dmin = min(heuristic)

        if dmin > thresh:
            return None
        else:
            return self[heuristic.index(dmin)]

    def new_frame(self, frame_nr=0):
        """Prepare all the trackers for a new frame. The taken switches are reset
        and cold trackers are removed

        Args:
            frame_nr (int): The new frame number

        """

        for person in self:
            # Drop trackers that have gone cold

            if not person.cold and frame_nr - person.last_seen > self.lost_threshold:
                person.cold = True
                if person.detection_count >= self.consecutive_frame_threshold:
                    self.manager_callback(person)

            if person.cold and person.detection_count < self.consecutive_frame_threshold:
                self.remove(person)

            person.taken = False

