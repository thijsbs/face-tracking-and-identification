"""A module to hande the video capture"""
import os
import sys
import cv2
import numpy as np
from tkinter import filedialog

import utils
import person
import classifier


class DetectionSystem(object):
    """The detector system class that governs one video stream and performs
    all the face detection operations on it.

    Args:
        cam_id (str): The camera ID for live situations in which case it should
            be an integer. For offline applications this specifies the filepath
            to the video file to analyze.
        trackers (PersonTrackerCollection): The person tracker collection
        known_persons (KnownPerson): A list of known person information to provide
            prediction class names.

    Attributes:
        cam_id (str): The camera ID for live situations in which case it should
            be an integer. For offline applications this specifies the filepath
            to the video file to analyze.
        trackers (PersonTrackerCollection): The person tracker collection
        known_persons (KnownPerson): A list of known person information to provide
            prediction class names.
        cap (VideoCapture): The openCV video capture object
        available (bool): True if the video stream is available. False otherwise
        img (numpy array): The current video frame
        frame_nr (int): The current frame number
        boxes (list[tensor]): A list of groups of two coordinates defining the position and 
        size of the face bounding box
        faces (list[PIL.Image]): A list of extracted face images in the current frame
        predictions (numpy array): The prediction tensors for all the faces in the frame
        tracker_ids (list[str]): A list of tracker ID's that are detected in the 
            current frame
        predicted_classes (list[int]): A list of class ID's detected in the 
            current frame
        predicted_confidences (list[float]): A list of prediction confidences
            of the predictions in the current frame

        """

    def __init__(self, cam_id, trackers, known_persons, enterexit=""):

        try:
            cam_id = int(cam_id)
        except ValueError:
            if cam_id == "file":
                cam_id = filedialog.askopenfilename(initialdir=__file__, title="Select the {} camera video".format(enterexit))
            pass

        self.cam_id = cam_id
        self.trackers = trackers
        self.known_persons = known_persons
        self.cap = cv2.VideoCapture(cam_id)
        self.available = self.cap.isOpened()
        self.img = None
        self.frame_nr = 0
        self.boxes = None
        self.faces = None
        self.predictions = None
        self.tracker_ids = []
        self.predicted_classes = None
        self.predicted_confidences = None


    def next_frame(self):
        """Prepare the attributes for processing the next frame"""
        if not self.available:
            return
        # self.img = None
        _, img = self.cap.read()
        if img is None:
            self.available = False
            self.img = None
            print(f"Camera {self.cam_id} is no longer available.")
            return
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.frame_nr += 1
        self.img = img
        self.boxes = None
        self.faces = None
        self.preditions = None
        self.tracker_ids = []
        self.predicted_classes = None
        self.predicted_confidences = None
        self.trackers.new_frame(self.frame_nr)


    def detect_faces(self, detector, thresh=0.7, bor=0.1):
        """Run the face detector on the current frame and extract
        all the faces that were detected

        Args:
            detector: The face detector model
            thresh (float): The face detection threshold
            bor (float): The fraction of additional border to add
                to the extracted face image

        """
        if not self.available:
            return None, None
        # The detector package prints outputs which I suppress without
        # changing the package itself
        with SuppressStdout(): 
            boxes, _, _ = detector.predict(self.img, 500, thresh)
        img_rgb = cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB)
        faces = [utils.crop_face(img_rgb, bx, bor) for bx in boxes]
        self.boxes = boxes
        self.faces = faces
 

    def classify(self, cnet):
        """Run the classifier model on all of the detected face images

        Args:
            cnet: The classifier model
        """
        if not self.available:
            return
        if cnet is None or not self.faces:
            return   
        predictions = classifier.predict(cnet, self.faces, batch=True)
        self.predictions = predictions


    def update_trackers(self, thresh=50):
        """Match each face observation with previously detected faces and
        update each tracker with these new observations

        Args: 
            thresh (int): The dissimilarity threshold between bounding boxes

        """
        if not self.available:
            return

        for i, box in enumerate(self.boxes):

            pred = None if self.predictions is None else self.predictions[i, :].squeeze()
            # Check if any previous face detections are close to this one
            tracked_person = self.trackers.query(box, pred, thresh=thresh)
            # If no trackers are close create a new tracking person
            if tracked_person is None:
                tracked_person = person.PersonTracker(box, pred, self.faces[i], frame_nr=self.frame_nr)
                self.trackers.append(tracked_person)
                self.tracker_ids.append(tracked_person.tracker_id)
            else:
                # Else update the existing tracker with the new detection
                tracked_person.update(box, self.faces[i], pred, frame_nr=self.frame_nr)
                self.tracker_ids.append(tracked_person.tracker_id)



    def prepare_frame(self, thresh=0.75, no_classify=False):
        """Prepare the current frame for displaying or saving by drawing each
        bounding box and class label to it

        Args:
            no_classify (bool): Should be True if the no_classification option is 
                used, in which case the tracker ID should be displayed as a label
        """

        if not self.available:
            return
        for i, box in enumerate(self.boxes):
            if no_classify:
                label = str(self.tracker_ids[i])

            else:
                tracker = self.trackers.get_tracker(self.tracker_ids[i])
                cid, conf = tracker.predict()
                if conf < thresh:
                    cid = 0
                name = self.known_persons[cid].first_name
                # name = self.known_persons[cid].last_name
                # label = "{} ({:.2f})".format(name, conf)
                label = "{} ({})".format(name, cid)

            # Draw the box
            cv2.rectangle(self.img, 
                          (box[0], box[1]), (box[2], box[3]), 
                          (0, 255, 0), 
                          4)

            cv2.putText(self.img, label, 
                        (box[0], box[1] - 10), # position
                        cv2.FONT_HERSHEY_SIMPLEX, # font
                        0.5,  # font scale
                        (0, 0, 255), #color
                        2)  # line type


    def show_frame(self, title=""):
        """Show the current frame in an openCV window. The key binding 'q'
        is used to exit the stream and close the window.

        Args:
            title (str): The title of the window
        """
        if not self.available:
            return False
        cv2.imshow(title, self.img)  
        if cv2.waitKey(1) & 0xFF == ord("q"):
            return True


    def save_frame(self, directory, filename="frame{0:05d}.jpg"):
        """Save the current frame with labels to an image file

        Args:
            directory (str): The directory where all frame images should be stored
            filename (str): The filename of the image file. The filename should have
                a fixed part and a formatting variable for adding a unique identifier.

        """
        if not self.available:
            return None

        cv2.imwrite(os.path.join(directory, filename.format(self.frame_nr)), self.img)


    def stop(self):
        """Stop the session and release the openCV objects"""
        if not self.available:
            return None

        self.cap.release()
        cv2.destroyAllWindows()


class SuppressStdout(object):
    """A class used to suppress print outputs where the source code should not
    be modified. This can be used as a context manager.
    """
    
    def __init__(self, suppress=True):
        self.suppress = suppress
        self.sys_stdout_ref = None

    def __enter__(self):
        self.sys_stdout_ref = sys.stdout
        if self.suppress:
            sys.stdout = open(os.devnull, 'w')
        return sys.stdout

    def __exit__(self, type, value, traceback):
        sys.stdout = self.sys_stdout_ref

