import os
import torch
import argparse
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
from PIL import Image
from torch.utils.data.dataset import Dataset
from torch.utils.data import random_split
from torchvision import models
from matplotlib import pyplot as plt

import person


parser = argparse.ArgumentParser(
    description='Run this as main to train the classifier')

parser.add_argument("-f", "--data_folder", required=True, type=str,
                    help="The directory of the data folder used for training")
parser.add_argument("-vf", "--validation_folder", help=("If a separate validation set is available add it here. "
                    "If not the training dataset will automatically be split to create a validation set."))
parser.add_argument("-c", "--csv", default="student_data.csv", type=str,
                    help="The path to the student information CSV file")
parser.add_argument("-s", "--save_path", default="classifier_weights.pt", type=str,
                    help="The file path where the trained model weights should be saved")
parser.add_argument("--checkpoint", help=("The path to a checkpoint file in case the training "
                                          "should be resumed from one"))
parser.add_argument("-bs", "--batch_size", default=64, type=int,
                    help="Hyperparameter: Batch size for the data loader")
parser.add_argument("-v", "--validation_fraction", default=0.1, type=float,
                    help="Hyperparameter: The fraction of the dataset to be used for validation")
parser.add_argument("-ep", "--epochs", default=10, type=int,
                    help="Hyperparameter: Number of epochs to train")
parser.add_argument("-lr", "--learning_rate", default=0.005, type=float,
                    help="Hyperparameter: Learning rate of the training")
parser.add_argument("--retrain_features", action="store_true",
                    help="Use if the feature weights should be re-trained. Default: False")
parser.add_argument('--model_type', default='squeezenet', const='squeezenet', nargs='?',
                    choices=['squeezenet', 'mobilenet', 'densenet', 'vgg16'],
                    help="The name of the model architecture to load. Default: squeezenet")


def get_trained_model(model_type, weights_fp, persons, device="cuda"):
    """Load in a model that was trained on the face data
    
    Args:
        model_type (str): The name of the model to load.
        weights_fp (str): The file path to the trained model weights file
        persons (list[KnownPerson]): A list of the known persons used to build the
            final classifier layer.
        device (str): The device type to use. This can be used to force the models 
            to CPU

    Returns:
        model: The trained model
    """
    model = define_new_model(model_type, persons, pretrained=False)
    model.load_state_dict(torch.load(weights_fp, map_location=torch.device(device)))
    return model


def define_new_model(model_type, persons, retrain_features=False, pretrained=True):
    """Load a pre-trained model architecture and adapt it to the available persons

    Args:  
        model_type (str): The name of the model to load.
        persons (list[KnownPerson]): A list of the known persons used to build the
            final classifier layer.
        retrain_features (bool): True if also the feature layers of the model should
            be re-trained

    Returns:
        The specified model architecture
    """


    if model_type == "squeezenet":
        model = models.squeezenet.squeezenet1_1(pretrained=pretrained)
        if not retrain_features:
            for param in model.features.parameters():
                param.require_grad = False
        model.classifier[1] = nn.Conv2d(512, len(persons), kernel_size=1)

    if model_type == "mobilenet":
        model = models.mobilenet.mobilenet_v2(pretrained=pretrained)
        if not retrain_features:
            for param in model.features.parameters():
                param.require_grad = False
        model.classifier[1] = nn.Linear(1280, len(persons))


    if model_type == "densenet":
        model = models.densenet.densenet201(pretrained=pretrained,
                                            memory_efficient=True)
        if not retrain_features:
            for param in model.features.parameters():
                param.require_grad = False
        model.classifier =  nn.Linear(1920, len(persons))

    if model_type == "inception":
        model = models.inception.inception_v3(pretrained=True)
        ## The pytorch inception implementation does not group its feature
        ## layers together. I could freeze each individual layer but since
        ## I will use it with retrain_features anyway I won't bother.
        # if not retrain_features:
        #     for param in model.features.parameters(): 
        #         param.require_grad = False
        model.fc = nn.Linear(2048, len(persons))


    if model_type == "vgg16":
        model = models.vgg.vgg16(pretrained=pretrained)
        if not retrain_features:
            for param in model.features.parameters():
                param.require_grad = False
        model.classifier =  nn.Linear(25088, len(persons))

    return model



def pre_process(img):
    """Preprocess an image for inference

    Args:
        img (PIL.Image): The face image to pre-process

    Returns:
        tensor: The pre-processed image
    """
    transf = transforms.Compose([transforms.Resize(224),
                                 transforms.ToTensor(),
                                 transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                                 ])
    return transf(img).unsqueeze(0)


def pre_process_batch(imgs):
    """Preprocess a batch of images for inference

    Args:
        imgs (list[PIL.Image]): The face images to pre-process

    Returns:
        tensor: The pre-processed image batch tensor
    """
    if not imgs:
        return None
    im_tensor = pre_process(imgs[0])
    for im in imgs[1:]:
        im_tensor = torch.cat([im_tensor, pre_process(im)], dim=0)
    return im_tensor


def predict(model, img, batch=False):
    """Classify the images

    Args:
        model: The trained model
        img (PIL.Image or list[PIL.Image]): The image or images to classify
        batch (bool): True if a batch of images should be used
    returns 
        numpy array: The prediction tensor of the image(s)
    """
    if model is None or img is None:
        return None
    if batch:
        face_tensor = pre_process_batch(img)
    else:
        face_tensor = pre_process(img)
    prediction = nn.functional.softmax(model(face_tensor)).cpu().detach().numpy()
    return prediction


class FaceDataset(Dataset):
    """A custom dataset to load in the face images with correct labels
    
    Args:
        folder_path (str): The directory where the images are stored
        people (list[KnownPerson]): A list of the person class names
        transform (torch.transform): A set of transformation to be applied
            to the dataset.

    Attributes:
        folder_path (str): The directory where the images are stored
        people (list[KnownPerson]): A list of the person class names
        transform (torch.transform): A set of transformation to be applied
            to the dataset.
        file_paths (list[str]): A list of all the known image file paths
            in the dataset
        labels (list[int]): A list of all the class labels in the dataset

    """
    def __init__(self, folder_path, people, transform=None):
        self.folder_path = folder_path
        self.people = people
        self.file_paths = []
        self.labels = []
        self.transform = transform if transform is not None else lambda x: x
        self._make_labeled_filenames()

    def _make_labeled_filenames(self):
        """Get all the available image file paths and pair them with the correct
        class labels.
        """
        face_folders = next(os.walk(self.folder_path))[1]
        known_names = [str(p).rstrip(" ") for p in self.people]
        missing_folders = [name for name in known_names if name not in face_folders]

        if missing_folders:
            msg = "Datafolders for {!r} are missing"
            raise IOError(msg.format(", ".join(missing_folders)))
        
        for name in known_names:
            face_dir = os.path.join(self.folder_path, name)
            files = [os.path.join(face_dir, fn) for fn in next(os.walk(face_dir))[2]]
            labels = [self.name2label(name)] * len(files)
            self.file_paths += files
            self.labels += labels

    def name2label(self, name):
        """Get the class ID from the class name

        Args:
            name (str): The class label name

        Returns:
            int: The corresponding class ID
        """
        if name == "Unknown":
            return 0
        for p in self.people:
            if str(p) == name:
                return p.clf_id
        return None

    def label2name(self, label):
        """Get the class name from the lass ID

        Args:
            label (int): The class ID

        Returns:
            str: The corresponding class label name
        """
        return str(self.people[label])

    def __getitem__(self, index):
        img_path = self.file_paths[index]
        label = self.labels[index]
        img = Image.open(img_path).convert("RGB")
        image_tensor = self.transform(img)
        label_tensor = torch.Tensor([label]).long()
        return image_tensor, label_tensor

    def __len__(self):
        return len(self.labels)


class DataSubset(Dataset):
    """A wrapper class for splitting the dataset into separate
    training and validation datasets with separate transforms

    Args:
        subset: A subset of the larger dataset
        transform (torch.transform): A set of transformation to be applied
            to the dataset.
    """
    def __init__(self, subset, transform=None):
        self.subset = subset
        self.transform = transform
        
    def __getitem__(self, index):
        x, y = self.subset[index]
        if self.transform:
            x = self.transform(x)
        return x, y
        
    def __len__(self):
        return len(self.subset)


def dataloaders(folder, known_persons, valid_folder=None, batch_size=20, valid_frac=0.2):
    """Define training and validation dataloaders for training.

    Args:
        folder (str): The directory where the training data is located
        known_persons (list[KnownPerson]): A list of the known persons
        valid_folder (str or None): The directory where the validation data is located.
            if None the training data will be split up into a training set and validation set.
        batch_size (int): The batch size to use during training
        valid_frac (float): The fraction of the training data to split off into a validation
            set. (if applicable)
    """

    #A dataset transform without data augmentation
    transf_valid = transforms.Compose([transforms.Resize(244),
                                       transforms.ToTensor(),
                                       transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                                       ])

    #The training dataset with data augmentation to squeeze out as many unique face images
    transf_train = transforms.Compose([
                                       transforms.RandomRotation(25),
                                       transforms.ColorJitter(brightness=0.8, contrast=0.2, saturation=0.05, hue=0.05),
                                       transforms.RandomResizedCrop(244, scale=(0.5, 1.5), ratio=(1., 1.)),
                                       transforms.ToTensor(),
                                       transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                                       ])

    if valid_folder is None:
        # If there is no specific validation set specified, split the training data
        face_dataset = FaceDataset(folder, known_persons)
        valid_size = int(valid_frac * len(face_dataset))
        train_size = len(face_dataset) - valid_size
        train_subset, valid_subset = random_split(face_dataset, [train_size, valid_size])

        train_dataset = DataSubset(train_subset, transform=transf_train)
        valid_dataset = DataSubset(valid_subset, transform=transf_valid)
    else:
        train_dataset = FaceDataset(folder, known_persons, transform=transf_train)
        valid_dataset = FaceDataset(valid_folder, known_persons, transform=transf_valid)

    data_loaders = {"train": torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True),
                    "valid": torch.utils.data.DataLoader(valid_dataset, batch_size=batch_size, shuffle=True)}

    return data_loaders


def criterion():
    """Define a loss criterion to use to optimize the model during training"""
    return nn.CrossEntropyLoss()


def optimizer(model, learning_rate=0.01):
    """Define an optimizer function for optimizing the model during training.

    Args:
        learning_rate (float): The learning rate during training
    """
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    return optimizer


def train(n_epochs, loaders, model, optimizer, criterion, use_cuda, save_path=None):
    """The main training loop where the model is being trained

    Args:
        n_epochs (int): The number of epoch to train the system
        loaders (dict): A dictionary containing the training and validation 
            dataloaders
        model: The model to train
        optimizer: The optimizer used for updating the model
        criterion: The loss criterion used for determining the loss of the model
        use_cuda (bool): True if a GPU should be used, False if the CPU should
            be used
        save_path (str): The file path where the trained model weights file should
            be saved.

    Returns:
        model: The trained model
        list[float]: training losses during training
        list[float]: validation losses during training
        list[float]: training accuracies during training
        list[float]: validation accuracies during training
    """
    # initialize tracker for minimum validation loss
    valid_loss_min = np.Inf 
    train_losses = []
    valid_losses = []
    train_acc = []
    valid_acc = []


    if use_cuda:
        model = model.cuda()

    for epoch in range(1, n_epochs+1):
        # initialize variables to monitor training and validation loss
        train_loss = 0.
        valid_loss = 0.
        ###################
        # train the model #
        ###################
        model.train()
        for batch_idx, (data, target) in enumerate(loaders['train']):
            # print(f"Epoch {epoch}, batch {batch_idx}")

            # move to GPU
            if use_cuda:
                data, target = data.cuda(), target.cuda()
            target = target.squeeze()
            ## find the loss and update the model parameters accordingly
            ## record the average training loss, using something like
            optimizer.zero_grad()
            output = model(data)
            loss = criterion(output, target)
            loss.backward()
            optimizer.step()
            train_loss = float(train_loss + ((1 / (batch_idx + 1)) * (loss.data - train_loss)))
            train_losses.append(float(train_loss))

            max_index = output.max(dim = 1)[1]
            correct = float((max_index == target).sum())
            train_acc.append(correct/target.shape[0]*100)

        ######################    
        # validate the model #
        ######################
        model.eval()
        for batch_idx, (data, target) in enumerate(loaders['valid']):
            # move to GPU
            if use_cuda:
                data, target = data.cuda(), target.cuda()
            target = target.squeeze()
            output = model(data)
            vloss = criterion(output, target)
            valid_loss = float(valid_loss + ((1 / (batch_idx + 1)) * (loss.data - valid_loss)))

            max_index = output.max(dim = 1)[1]
            correct = float((max_index == target).sum())
            valid_acc.append(correct/target.shape[0]*100)

            ## update the average validation loss
        valid_losses.append(float(valid_loss))
        print('Epoch: {} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f}'.format(
            epoch, 
            train_loss,
            valid_loss
            ))
        
        # save the model if validation loss has decreased
        if valid_loss <= valid_loss_min and save_path is not None:
            print("Validation loss decreased... Saving model")
            torch.save(model.state_dict(), save_path)
            valid_loss_min = valid_loss

    return model, train_losses, valid_losses, train_acc, valid_acc


if __name__ == "__main__":
    args = parser.parse_args()

    persons = person.known_person_list(args.csv)

    dataloaders = dataloaders(args.data_folder, 
                              persons, 
                              args.validation_folder,
                              args.batch_size, 
                              args.validation_fraction)
    model = define_new_model(args.model_type, persons, args.retrain_features)
    if args.checkpoint is not None:
        model.load_state_dict(torch.load(args.checkpoint))

    criterion = criterion()
    optimizer = optimizer(model, learning_rate=args.learning_rate)

    gpu_available = torch.cuda.is_available()
    
    print("Starting training process...")
    model, train_loss, valid_loss, train_acc, valid_acc = train(args.epochs, 
                                                                dataloaders,
                                                                model, 
                                                                optimizer, 
                                                                criterion, 
                                                                gpu_available, 
                                                                save_path=args.save_path)
    x_train = np.linspace(0., args.epochs, len(train_loss))
    x_valid = np.linspace(0., args.epochs, len(valid_loss))
    plt.plot(x_train, train_loss, "r")
    plt.plot(x_valid, valid_loss, "b")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend(["Training loss", "Validation loss"])
    plt.savefig("training_losses.jpg")

    plt.figure(2)
    x_train = np.linspace(0., args.epochs, len(train_acc))
    x_valid = np.linspace(0., args.epochs, len(valid_acc))
    plt.plot(x_train, train_acc, "r")
    plt.plot(x_valid, valid_acc, "b")
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy %")
    plt.legend(["Training accuracy", "Validation accuracy"])
    plt.savefig("training_accuracy.jpg")

    plt.show()
