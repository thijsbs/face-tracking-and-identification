"""Run the main loop of the program
"""
import os
import argparse
import cv2
import shutil
from datetime import datetime

import person
import classifier
import utils
import communications
import detection_system
from ULFGFD.vision.ssd.config import fd_config
from ULFGFD.vision.ssd import mb_tiny_RFB_fd as RFB

detector_model = "ULFGFD/models/pretrained/version-RFB-320.pth"


# Define the command line arguments that can be used to finetune
# The system
parser = argparse.ArgumentParser(
    description="Run the schoolbus face detection algorithm")

parser.add_argument("--test_device", default="cuda:0", type=str,
                    help=("Choose to run the model on GPU (cuda:0) "
                          "or CPU (cpu). Default: cuda:0"))

parser.add_argument('--detector_path', 
                    default="ULFGFD/models/pretrained/version-RFB-320.pth",
                    type=str,
                    help=("Path to the face detector weights file. Default: "
                          "ULFGFD/models/pretrained/version-RFB-320.pth"))

parser.add_argument('--classifier_path', default="classifier_weights.pt", type=str,
                    help=("Path to the classifier weights file. Default: "
                          "classifier_weights.pt"))

parser.add_argument("-c1", "--enter_camera", default="x", type=str,
                    help=("The enter camera id (single integer corresponding to the "
                          "system camera), or path to the enter camera video "
                          "source for offline testing. If the keyword 'file' is given, a "
                          "GUI dialog will ask to pick the file. By default a false filename "
                          "is given so that no video stream is loaded automatically. "
                          "Default: x"))

parser.add_argument("-c2", "--exit_camera", default="x", type=str,
                    help=("The exit camera id (single integer corresponding to the "
                          "system camera), or path to the exit camera video "
                          "source for offline testing. If the keyword 'file' is given a "
                          "GUI dialog will ask to pick the file. DBy default a false filename "
                          "is given so that no video stream is loaded automatically. "
                          "Default: x"))

parser.add_argument("--no_classification", action="store_true", 
                    help=("Use this flag if you want to run the tracking without "
                          "doing classification. Default: False"))

parser.add_argument("-c", "--csv", default="student_data.csv", type=str,
                    help=("The path to the student information CSV file. "
                          "Default: student_data.csv"))

parser.add_argument("--enter_threshold", default=10, type=int,
                    help=("The number of frames a person has to be deteted "
                          "before the person is considered as 'entered'. "
                          "Default: 10"))

parser.add_argument("--exit_threshold", default=10, type=int,
                    help=("The number of frames a person has to be deteted "
                          "before the person is considered as 'exited'. "
                          "Default: 10"))

parser.add_argument("--box_similarity_threshold", default=50, type=int,
                    help=("A theshold defining how distant the bounding box points "
                          "are allowed to be compared to the previous frame to count "
                          "the bounding box as belonging to the same person. "
                          "Default: 50"))

parser.add_argument('--save_frames', action="store_true", 
                    help=("Use this flag if you want to save the processed video frames "
                          "as images. The output directory is set with '--output_dir'. "
                          "Default: False"))

parser.add_argument('--output_dir', default="out_frames", type=str,
                    help=("Where to store the output frames of the processed "
                          "video. Default: out_frames/."))

parser.add_argument('--save_faces', action="store_true", help="Save the extracted faces.")

parser.add_argument('--faces_dir', default="extracted_faces", type=str,
                    help="Where to store extracted face images. Default: extracted_faces/")

parser.add_argument("--face_detection_threshold", default=0.7, type=float,
                    help=("The detection threshold of the face detector. "
                          "Default: 0.7."))

parser.add_argument("--classification_threshold", default=0.5, type=float,
                    help=("The theshold of the classification prediction below "
                          "which the classification is considered as 'unknown'. "
                          "Default: 0.5"))

parser.add_argument("--frame_continuity_threshold", default=50, type=int,
                    help=("The number of frames where no new detection is made before the tracker "
                          "can be considered lost. Default: 50"))

parser.add_argument('--face_output_border', default=0.1, type=float,
                    help=("The border around the face bounding box. This is the "
                          "fraction of the size of the original box. Default: 0.1."))

parser.add_argument("--no_show", action="store_true", 
                    help="Suppress showing the video stream. Default: False")

parser.add_argument("--fps", action="store_true", 
                    help="Print the fps of the system loop. Default: False")

parser.add_argument("--sequential_streams", action="store_true", 
                    help=("Use this flag if the exit stream should be started after the "
                          "enter stream has been stopped. This is useful for offline testing. "
                          "If False both the enter and exit streams will play simultaneously. "
                          "Default: False"))

parser.add_argument("--require_message_confirmation", action="store_true", 
                    help=("Use this fag if messages sent to the parents should need "
                          "a confirmation before sending. Default: False"))

parser.add_argument("--messaging", action="store_true", 
                    help="Use this flag if messaging should be enables. Default: False")

parser.add_argument('--model_type', default='squeezenet', const='squeezenet', nargs='?',
                    choices=['squeezenet', 'mobilenet', 'densenet', 'vgg16'],
                    help="The name of the model architecture to load. Default: squeezenet")

args = parser.parse_args()


# Load in the trained RFB face detecor network
fd_config.define_img_size(480)
detector_net = RFB.create_Mb_Tiny_RFB_fd(2, is_test=True, 
                                         device=args.test_device)
face_predict = RFB.create_Mb_Tiny_RFB_fd_predictor(detector_net, 
                                                   candidate_size=1000, 
                                                   device=args.test_device)
detector_net.load(args.detector_path)


# Initialize the person tracking data structure
known_persons = person.known_person_list(args.csv)
manager = communications.Manager(known_persons, 
                                 args.classification_threshold,
                                 args.messaging,
                                 args.require_message_confirmation)

trackers_enter = person.PersonTrackerCollection(manager.add_enter_observation, args.enter_threshold)
trackers_exit = person.PersonTrackerCollection(manager.add_exit_observation, args.exit_threshold)

# Load in the face recognizer classifier network
cnet = None
if not args.no_classification:
    cnet = classifier.get_trained_model(args.model_type,
                                        args.classifier_path,
                                        known_persons, 
                                        args.test_device)

# Initialize video stream and output directories
outdir_enter = os.path.join(args.output_dir, "enter")
outdir_exit = os.path.join(args.output_dir, "exit")

# if saving frame output, first remove old results to avoid confusion
if args.save_frames:
    if os.path.isdir(outdir_enter):
        shutil.rmtree(outdir_enter)
    os.makedirs(outdir_enter)

    if os.path.isdir(outdir_exit):
        shutil.rmtree(outdir_exit)
    os.makedirs(outdir_exit)

face_dir_enter = os.path.join(args.faces_dir, "enter")
face_dir_exit = os.path.join(args.faces_dir, "exit")
if args.save_faces:
    if os.path.isdir(face_dir_enter):
        shutil.rmtree(face_dir_enter)
    os.makedirs(face_dir_enter)
    if os.path.isdir(face_dir_exit):
        shutil.rmtree(face_dir_exit)
    os.makedirs(face_dir_exit)


# Set up the camera system
# Determine if the camera inputs are live camera's (in which case
# they should be integers, or offline video files)
cam_enter = detection_system.DetectionSystem(args.enter_camera, trackers_enter, known_persons, "enter")
cam_exit = detection_system.DetectionSystem(args.exit_camera, trackers_exit, known_persons, "exit")

if not cam_enter.available and not cam_exit.available:
    print("No camera or video source detected")
    quit()

if not cam_enter.available:
    print("No camera or video source detected for the enter camera")

if not cam_exit.available:
    print("No camera or video source detected for the exit camera")

# Initialize the initial frames
exit_stream_disabled = False
if cam_enter.available and cam_exit.available and args.sequential_streams:
  cam_exit.available = False
  exit_stream_disabled = True  

# Start the main loop
while not manager.arrived: #manager.arrived can be set in the future via an application
    frame_start = datetime.now()
    
    # First process the entrance stream
    cam_enter.next_frame()
    cam_enter.detect_faces(face_predict, args.face_detection_threshold, args.face_output_border)
    cam_enter.classify(cnet)
    cam_enter.update_trackers(args.box_similarity_threshold)

    if exit_stream_disabled and not cam_enter.available:
      cam_exit.available = True

    # Process the exit stream
    cam_exit.next_frame()
    cam_exit.detect_faces(face_predict, args.face_detection_threshold, args.face_output_border)
    cam_exit.classify(cnet)
    cam_exit.update_trackers(args.box_similarity_threshold)

    if cam_enter.img is None and cam_exit.img is None:
        print("End of the session.")
        manager.arrived = False
        break

    if not args.no_show or args.save_frames:
        cam_enter.prepare_frame(args.classification_threshold, args.no_classification)
        cam_exit.prepare_frame(args.classification_threshold, args.no_classification)


    if not args.no_show:
        stop1 = cam_enter.show_frame(title="Enter camera")
        stop2 = cam_exit.show_frame(title="Exit camera")
        if stop1 or stop2:
            break

    if args.save_frames:
        cam_enter.save_frame(outdir_enter)
        cam_exit.save_frame(outdir_exit)

    if args.fps:
        frame_time = datetime.now()-frame_start
        print("FPS: {:.1f}".format(1/frame_time.total_seconds()))

manager.final_stop()
cam_enter.stop()
cam_exit.stop()

if args.save_faces:
    for p in trackers_enter:
        if p.detection_count < args.enter_threshold:
            continue

        p.save_faces(face_dir_enter)
    for p in trackers_exit:
        if p.detection_count < args.exit_threshold:
            continue
        p.save_faces(face_dir_exit)

print("The system has finished")
