from PIL import Image


def crop_face(img, bbox, border=0.1):
    """Extract the face from the frame based on the detected bounding box.
    An additional border can be specified to include more of the face.
    The final image is made into a square by changing the indices if possible
    if not it will pad the image.

    Args:
        bbox (tensor):Two coordinates defining the position and size of 
            the current face bounding box
        border (float): The border around the face bounding box. This is the
            fraction of the size of the original box

    Returns:
        PIL.Image: The extracted square face image
    """
    h, w, _ = img.shape
    x1, x2, y1, y2 = square_indices(bbox, border)
    if x1 < 0 or x2 > w or y1 < 0 or y2 > h:
        # If one of the indexes are out of bounds use a
        # padding method for creating square face images
        x1 = max(0, min(int(bbox[0]), w))
        x2 = max(0, min(int(bbox[2]), w))
        y1 = max(0, min(int(bbox[1]), h))
        y2 = max(0, min(int(bbox[3]), h))
        face = Image.fromarray(img[y1:y2, x1:x2, :])
        face = make_square(face)
    else:
        # Use the indexing method for creating square images
        face = Image.fromarray(img[y1:y2, x1:x2, :])

    face = face.resize((128, 128), Image.ANTIALIAS)

    return face


def square_indices(bbox, border=0.1):
    """Change the bounding box indices to make the final extracted image square

    Args:
        bbox (tensor):Two coordinates defining the position and size of 
            the current face bounding box
        border (float): The border around the face bounding box. This is the
            fraction of the size of the original box

    Returns:
        tuple: The new square box indices x1, x2, y1, y2
    """
    w = float(bbox[2] - bbox[0])
    h = float(bbox[3] - bbox[1])

    if min((w, h)) == w:
        diff = (h-w)/2 + border*h/2
        x1 = int(bbox[0] - diff)
        x2 = int(bbox[2] + diff)
        y1 = int(bbox[1] - border*h/2)
        y2 = int(bbox[3] + border*h/2)
    else:
        diff = (w-h)/2 + border*w/2
        x1 = int(bbox[0] - border*w/2)
        x2 = int(bbox[2] + border*w/2)
        y1 = int(bbox[1] - diff)
        y2 = int(bbox[3] + diff)
    return x1, x2, y1, y2



def make_square(img):
    """Take a rectangular image and make it square by padding it with grey pixels

    Args:
        img (PIL.Image): The original rectangular image

    Returns:
        PIL.Image: The padded square, image
    """
    
    fill_color = (128, 128, 128)
    x, y = img.size
    size = max(img.size)
    new_img = Image.new('RGB', (size, size), fill_color)
    new_img.paste(img, (int((size - x) / 2), int((size - y) / 2)))
    return new_img